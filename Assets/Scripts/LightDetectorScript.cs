﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

public class LightDetectorScript : MonoBehaviour {



	private float gaussianOutput,linearOutput,invertedLinearOutput,gaussianAux;
	private float altmax;
	private int numObjects;

	public float angle;
	public Boolean isGaussian;

    public float bias;
    public float center;
    public float desvio;
    public float limiteSuperior;
    public float limiteInferior;
    public float limiarSuperior;
    public float limiarInferior;

    void Start () {
        linearOutput=0;
        gaussianOutput=0;
        invertedLinearOutput=0;
        numObjects = 0;
	}

	void Update () {
		GameObject[] lights = GetVisibleLights ();
        Linear(lights);
        Gaussian(lights);
        InvertedLinear(lights);
	}

	public float getOutput(){
		if (isGaussian)
			return getGaussianOutput();
		else
			return getLinearOutput();
	}
    void Gaussian(GameObject[] lights)
    {
        gaussianOutput = 0;
		if (center != 0 && desvio != 0) {
			altmax = (float)(1 / (center * Math.Sqrt (2 * Math.PI)));

			gaussianOutput = altmax * Mathf.Exp (-(Mathf.Pow ((gaussianAux - center), 2)) / (2 * Mathf.Pow (desvio, 2)));
		}
    }

    void Linear(GameObject[] lights)
    {

        linearOutput = 0;
        numObjects = lights.Length;

        foreach (GameObject light in lights)
        {
            float r = light.GetComponent<Light>().range;
            if (Vector3.Distance(transform.position, light.transform.position) < 30)
            {
                Debug.DrawLine(transform.position, light.transform.position, Color.green);
                linearOutput += 1f / Mathf.Pow((transform.position - light.transform.position).magnitude / r + 1, 2);
                
            }
        }
        if (numObjects > 0)
            linearOutput = linearOutput / numObjects;
        gaussianAux = linearOutput;
    }

    void InvertedLinear(GameObject[] lights)
    {
        invertedLinearOutput = (1 - linearOutput);
    }

	// Get Sensor output value
	float getLinearOutput(){
        if(linearOutput>limiarSuperior)
            linearOutput = limiteInferior;
        if(linearOutput<limiarInferior)
            linearOutput = limiteInferior;
        linearOutput*=bias;
        if (linearOutput > limiteSuperior)
            linearOutput = limiteSuperior;
        if (linearOutput < limiteInferior)
            linearOutput = limiteInferior;
        return linearOutput;
	}

    float getGaussianOutput(){

        if(gaussianOutput > limiarSuperior)
            gaussianOutput = limiteInferior;
        if(gaussianOutput < limiarInferior)
            gaussianOutput = limiteInferior;
        gaussianOutput*=bias;
        if (gaussianOutput > limiteSuperior)
            gaussianOutput = limiteSuperior;
        if (gaussianOutput < limiteInferior)
            gaussianOutput = limiteInferior;
        return gaussianOutput;
    }

    public float getInvertedLinearOutput(){
        if(invertedLinearOutput > limiarSuperior)
            invertedLinearOutput = limiteInferior;
        if(invertedLinearOutput < limiarInferior)
            invertedLinearOutput = limiteInferior;
        invertedLinearOutput*=bias;
        if (invertedLinearOutput > limiteSuperior)
            invertedLinearOutput = limiteSuperior;
        if (invertedLinearOutput < limiteInferior)
            invertedLinearOutput = limiteInferior;

        if (invertedLinearOutput == 0)
            return 0;
        else
            return (1-linearOutput);
    }
		
	// Returns all "Light" tagged objects. The sensor angle is not taken into account.
	GameObject[] GetAllLights()
	{
		return GameObject.FindGameObjectsWithTag ("Light");
	}

	// Returns all "Light" tagged objects that are within the view angle of the Sensor. Only considers the angle over 
	// the y axis. Does not consider objects blocking the view.
	GameObject[] GetVisibleLights()
	{
		ArrayList visibleLights = new ArrayList();
		float halfAngle = angle / 2.0f;

		GameObject[] lights = GameObject.FindGameObjectsWithTag ("Light");

		foreach (GameObject light in lights) {
			Vector3 toVector = (light.transform.position - transform.position);
			Vector3 forward = transform.forward;
			toVector.y = 0;
			forward.y = 0;
			float angleToTarget = Vector3.Angle (forward, toVector);

			if (angleToTarget <= halfAngle) {
				visibleLights.Add (light);
			}
		}

		return (GameObject[])visibleLights.ToArray(typeof(GameObject));
	}


}
