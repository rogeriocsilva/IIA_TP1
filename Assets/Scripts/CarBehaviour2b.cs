﻿using UnityEngine;
using System.Collections;

public class CarBehaviour2b : CarBehaviour {
	
	void Update()
	{
		//Read sensor values
		float leftSensor = LeftLD.getOutput();
		float rightSensor = RightLD.getOutput();
		float leftBlockSensor = LeftBD.getLinearOutput ();
        float rightBlockSensor = RightBD.getLinearOutput();

        

        //Calculate target motor values
        m_RightWheelSpeed = leftSensor * MaxSpeed;
		m_LeftWheelSpeed = rightSensor * MaxSpeed;
		m_RightWheelSpeed += rightBlockSensor * MaxSpeed;
        m_LeftWheelSpeed += leftBlockSensor * MaxSpeed; 
	}
}
