﻿using UnityEngine;
using System.Collections;

public class CarBehaviour3a : CarBehaviour
{

    void Update()
    {
        //Read sensor values
		float leftSensor = LeftLD.getInvertedLinearOutput();
		float rightSensor = RightLD.getInvertedLinearOutput();
        float leftBlockSensor = LeftBD.getLinearOutput();
        float rightBlockSensor = RightBD.getLinearOutput();

        

       

        //Calculate target motor values
        m_LeftWheelSpeed = leftSensor * MaxSpeed;
        m_RightWheelSpeed = rightSensor * MaxSpeed;
        m_RightWheelSpeed += rightBlockSensor * MaxSpeed;
        m_LeftWheelSpeed += leftBlockSensor * MaxSpeed;
    }
}
