﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    public GameObject prefab;
    public int n;
    public GameObject carro;
    public double y = 0.5;

    void Start()
    {
        for (int i = 0; i < n; i++)
        {
            SpawnRandomCube();
        }
        SpawnWalls();
    }

    void SpawnWalls()
    {
        if (prefab.tag == "Bloco")
        {

            for (int i = 0; i < 50; i++)
            {
                Vector3 pos = new Vector3(-24.5f + (i), (float)y, -24.5f);
                Instantiate(prefab, pos, Quaternion.identity);
                pos = new Vector3(-24.5f, (float)y, -24.5f + i);
                Instantiate(prefab, pos, Quaternion.identity);
                pos = new Vector3(-24.5f + i, (float)y, 24.5f);
                Instantiate(prefab, pos, Quaternion.identity);
                pos = new Vector3(24.5f, (float)y, -24.5f + i);
                Instantiate(prefab, pos, Quaternion.identity);
            }
        }
    }

    void SpawnRandomCube() { 
		Vector3 pos = new Vector3 (Random.Range (-25.0F, 25.0F), (float) y, Random.Range (-25F, 25.0F));


		if (pos.x > carro.transform.position.x - 2 && pos.x < carro.transform.position.x +2 && pos.z > carro.transform.position.z - 2 && pos.z < carro.transform.position.z + 2)
        {

            pos = new Vector3(Random.Range(carro.transform.position.x + 2 , 25.0F), (float)y, Random.Range(carro.transform.position.z + 2,25.0F));

        }
        Instantiate(prefab, pos, Quaternion.identity);
    }
}
