﻿using UnityEngine;
using System.Collections;

public class BlockDetector : MonoBehaviour {
	public float output;
    public float angle;
	RaycastHit hitInfo;
    public float radius;


	// Use this for initialization
	void Start () {
        radius = 3f;
		output = 0;
        angle = 90;
	}
	
	// Update is called once per frame
	void Update () {
		output = 0;
        int i = 0;
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius); /*obter todos os objetos em roda do sensor*/
        float near = 1000;
        while (i < hitColliders.Length)
        {
            if (hitColliders[i].tag == "Bloco") /*restring os objetos aos blocos*/
            {
                float halfAngle = angle / 2.0f;
                Vector3 toVector = (hitColliders[i].transform.position - transform.position);
                Vector3 forward = transform.forward;
                toVector.y = 0;
                forward.y = 0;
                float angleToTarget = Vector3.Angle(forward, toVector);

				Debug.DrawLine (transform.position, hitColliders[i].transform.position, Color.red);
                if (angleToTarget <= halfAngle) /*verifica se esta dentro do angulo definido*/
                {
					
                    float distance = Vector3.Distance(hitColliders[i].transform.position, transform.position);
                    if (distance < near)/*determina o mais proximo*/
                    {
                        near = distance;
                    }
                    
                }


            }

            i++;
        }
        if(near == 1000){/*caso não haja nenhum cubo em vista*/
            output = 0;
        }
        else
            output =( 1 / (Mathf.Pow(near, 2)) + 0.5f);

    }

	public float getLinearOutput(){

		return output;
	}
}
